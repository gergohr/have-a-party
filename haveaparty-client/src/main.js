import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store'
import VueResource from 'vue-resource'
import DatetimePicker from 'vuetify-datetime-picker'
import 'vuetify-datetime-picker/src/stylus/main.styl'

Vue.config.productionTip = false

Vue.use(VueResource);
Vue.use(DatetimePicker);

Vue.http.options.root = 'http://localhost:8080/';

Vue.http.interceptors.push((request, next) => {
  request.headers.set('Authorization', localStorage.getItem("token"))
  next()
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

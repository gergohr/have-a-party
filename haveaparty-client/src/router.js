import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import UserEdit from '@/views/UserEdit.vue'
import UserView from '@/views/UserView.vue'
import Event from '@/views/Event.vue'
import EventEdit from '@/views/EventEdit.vue'
import Events from '@/views/Events.vue'
import Login from '@/views/Login.vue'
import Register from '@/views/Register.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    { path: '/', name: 'home', component: Home },
    { path: '/login', name: 'login', component: Login},
    { path: '/register', name: 'register', component: Register},
    { path: '/edit/:id', name: 'edit', component: UserEdit },
    { path: '/view/:username', name: 'view', component: UserView },
    { path: '/events/:id', name: 'event', component: Event },
    { path: '/events/:id/edit', name: 'eventEdit', component: EventEdit },
    { path: '/events', name: 'events', component: Events },
    {
      path: '/about', 
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    
    { path: '*', redirect: '/' },
  ]
})

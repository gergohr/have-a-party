import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    darkMode: true,
    isLoggedIn: !!localStorage.getItem("token")
  },
  getters: {
    darkMode: state => state.darkMode,
    isLoggedIn: state => state.isLoggedIn
  },
  mutations: {
    changeDarkMode: state => state.darkMode = !state.darkMode,
    loginSuccess: state => state.isLoggedIn = true,
    logoutSuccess: state => state.isLoggedIn = false
  },
  actions: {
    changeDarkMode: ({commit}) => commit('changeDarkMode'),
    login: ({commit}, token) => {
      localStorage.token = token
      if(localStorage.token){
        commit('loginSuccess')
      }
    },
    logout: ({commit}) => {
      localStorage.removeItem("token")
      if(!localStorage.token){
        commit('logoutSuccess')
      }
    }
  }
})

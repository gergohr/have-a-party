insert into users (creation_date, last_modification_date, about, city, country, email, first_name, instagram, last_name, nick_name, password, job, age) values (current_timestamp, current_timestamp,'Nincs mit moondanom!','Debrecen','Magyarország','feri@gmail.hu','Ferenc','ferinsta','Kis','Fecó','$2a$10$vhOzrT0kX9DNnpxlUuNhRuCGzZOk8N4m50uWaL2D8.q9dTSpSd1QK', 'tanuló', 17);
insert into users (creation_date, last_modification_date, about, city, country, email, first_name, instagram, last_name, nick_name, password, job, age) values (current_timestamp, current_timestamp,'Gábor vagyok, Debrecenből.','Debrecen','Magyarország','gabi@gmail.hu','Gábor','gabinsta','Balogh','Gaben','$2a$10$vhOzrT0kX9DNnpxlUuNhRuCGzZOk8N4m50uWaL2D8.q9dTSpSd1QK', 'tanuló', 19);
insert into users (creation_date, last_modification_date, about, city, country, email, first_name, instagram, last_name, nick_name, password, job, age) values (current_timestamp, current_timestamp,'Tamás vagyok és Pesten élek.','Budapest','Magyarország','tomi@gmail.hu','Tamás','ferinsta','Nagy','Tom','$2a$10$vhOzrT0kX9DNnpxlUuNhRuCGzZOk8N4m50uWaL2D8.q9dTSpSd1QK', 'Tanár', 32);
insert into users (creation_date, last_modification_date, about, city, country, email, first_name, instagram, last_name, nick_name, password, job, age) values (current_timestamp, current_timestamp,'Hát... semmi extra.','Vác','Magyarország','irma@gmail.hu','Irma','ferinsta','Tóth','Irmu','$2a$10$vhOzrT0kX9DNnpxlUuNhRuCGzZOk8N4m50uWaL2D8.q9dTSpSd1QK', 'Szoftvertesztelő', 23);

insert into comment (creation_date, last_modification_date, content, user_writer_id) values (current_timestamp, current_timestamp,'nagyon várjuk már!', 4);
insert into comment (creation_date, last_modification_date, content, user_writer_id) values (current_timestamp, current_timestamp,'mi is!', 3);
insert into comment (creation_date, last_modification_date, content, user_writer_id) values (current_timestamp, current_timestamp,'glutén mentes ételek lesznek?', 4);
insert into comment (creation_date, last_modification_date, content, user_writer_id) values (current_timestamp, current_timestamp,'Igen, hamarosan láthatóvá válik az is.', 2);
insert into comment (creation_date, last_modification_date, content, user_writer_id) values (current_timestamp, current_timestamp,'Köszi, alig várom.', 4);
insert into comment (creation_date, last_modification_date, content, user_writer_id) values (current_timestamp, current_timestamp,'Hajrá!', 3);

insert into event (creation_date, last_modification_date, title, description, begins_at, ends_at, country, city, street, street_number, price, user_organizer_id) values (current_timestamp, current_timestamp, 'Különleges hiperbuli', 'Különleges hiperbuli a hazai legnyagyobb előadókkal. Részletek később...','2019-10-10 22:00:00','2019-10-11 04:11:00','Magyarország','Debrecen','Piac utca', '18',3000,1);
insert into event (creation_date, last_modification_date, title, description, begins_at, ends_at, country, city, street, street_number, price, user_organizer_id) values (current_timestamp, current_timestamp, 'Fantasztikus humorfesztivál', 'Fantasztikus humorfesztivál a legismertebb humoristákkal.','2019-10-12 18:00:00','2019-10-12 21:00:00','Magyarország','Debrecen','Miklós utca', '11',3000,1);
insert into event (creation_date, last_modification_date, title, description, begins_at, ends_at, country, city, street, street_number, price, user_organizer_id) values (current_timestamp, current_timestamp, 'Az év bábszínháza', 'Elhozzuk nektek a valaha volt legnagyobb bábszínházat.','2019-10-22 08:00:00','2019-10-22 19:00:00','Magyarország','Debrecen','Kossuth utca', '1',4000,1);
insert into event (creation_date, last_modification_date, title, description, begins_at, ends_at, country, city, street, street_number, price, user_organizer_id) values (current_timestamp, current_timestamp, 'Falunap', 'Világhírű sztárvendégek és még sok minden más.','2019-12-10 22:00:00','2019-12-11 04:00:00','Magyarország','Debrecen','Veres Péter utca', '81',5000,2);
insert into event (creation_date, last_modification_date, title, description, begins_at, ends_at, country, city, street, street_number, price, user_organizer_id) values (current_timestamp, current_timestamp, 'Sulinyitó bál', 'Korlátlan étel és italfogyasztás...(Menü: Palóc leves, Rántott szelet hasábburgonyával, Krémes','2019-09-04 18:00:00','2019-09-05 04:00:00','Magyarország','Debrecen','Szent Anna utca', '23',6000,2);

insert into event_guests (event_id, guests_id) VALUES (1,2);
insert into event_guests (event_id, guests_id) VALUES (1,3);
insert into event_guests (event_id, guests_id) VALUES (1,4);
insert into event_guests (event_id, guests_id) VALUES (2,1);
insert into event_guests (event_id, guests_id) VALUES (2,3);
insert into event_guests (event_id, guests_id) VALUES (3,2);
insert into event_guests (event_id, guests_id) VALUES (4,1);
insert into event_guests (event_id, guests_id) VALUES (4,4);
insert into event_guests (event_id, guests_id) VALUES (5,1);
insert into event_guests (event_id, guests_id) VALUES (5,4);

insert into event_comments (event_id, comments_id) values (1,1);
insert into event_comments (event_id, comments_id) values (1,2);
insert into event_comments (event_id, comments_id) values (5,3);
insert into event_comments (event_id, comments_id) values (5,4);
insert into event_comments (event_id, comments_id) values (5,5);
insert into event_comments (event_id, comments_id) values (2,6);

insert into list_of_performers (event_id, performers) values (1,'David Guetta');
insert into list_of_performers (event_id, performers) values (1,'Dublic');
insert into list_of_performers (event_id, performers) values (2,'Annamary');
insert into list_of_performers (event_id, performers) values (2,'Kocács András Péter');
insert into list_of_performers (event_id, performers) values (3,'Stohl András');
insert into list_of_performers (event_id, performers) values (4,'Annamary');
insert into list_of_performers (event_id, performers) values (5,'Whitewater');


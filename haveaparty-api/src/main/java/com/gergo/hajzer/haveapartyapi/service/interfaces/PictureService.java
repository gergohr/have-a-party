package com.gergo.hajzer.haveapartyapi.service.interfaces;

import com.gergo.hajzer.haveapartyapi.entity.Picture;
import org.springframework.web.multipart.MultipartFile;

public interface PictureService {

    Picture savePicture(Picture picture);

    Picture getPicture(long id);

    void deletePictureById(long id);

    Picture makePicture(MultipartFile file);
}

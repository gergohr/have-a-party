package com.gergo.hajzer.haveapartyapi.entity;

import com.gergo.hajzer.haveapartyapi.enums.Roles;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@RequiredArgsConstructor
@NoArgsConstructor
@Table(name = "users")
public class User extends EntityBase {

    @NonNull
    private String email;

    @NonNull
    private String firstName;

    @NonNull
    private String lastName;

    @NonNull
    private String nickName;

    @NonNull
    private String city;

    @NonNull
    private String country;

    @NonNull
    private String about;

    private String job;

    private int age;

    private String instagram;

    @NonNull
    private String password;

    @OneToOne
    private Picture profilePicture;
}

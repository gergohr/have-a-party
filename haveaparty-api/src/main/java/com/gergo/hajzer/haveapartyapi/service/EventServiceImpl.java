package com.gergo.hajzer.haveapartyapi.service;

import com.gergo.hajzer.haveapartyapi.dto.event.*;
import com.gergo.hajzer.haveapartyapi.dto.partyhashtag.PartyHashtagDto;
import com.gergo.hajzer.haveapartyapi.dto.user.UserDto;
import com.gergo.hajzer.haveapartyapi.entity.Event;
import com.gergo.hajzer.haveapartyapi.entity.PartyHashtag;
import com.gergo.hajzer.haveapartyapi.entity.User;
import com.gergo.hajzer.haveapartyapi.exception.NotFoundEventResourceException;
import com.gergo.hajzer.haveapartyapi.exception.NotFoundPartyHashtagResourceException;
import com.gergo.hajzer.haveapartyapi.exception.NotFoundUserResourceException;
import com.gergo.hajzer.haveapartyapi.repository.EventRepository;
import com.gergo.hajzer.haveapartyapi.repository.PartyHashtagRepository;
import com.gergo.hajzer.haveapartyapi.repository.UserRepository;
import com.gergo.hajzer.haveapartyapi.service.interfaces.EventService;
import com.gergo.hajzer.haveapartyapi.service.interfaces.PictureService;
import com.gergo.hajzer.haveapartyapi.service.interfaces.UserService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.lang.reflect.Type;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Data
@RequiredArgsConstructor
public class EventServiceImpl implements EventService {

    private final EventRepository eventRepository;
    private final UserRepository userRepository;
    private final PartyHashtagRepository partyHashtagRepository;
    private final ModelMapper modelMapper;

    private final PictureService pictureService;

    @Override
        public EventDto createEvent(CreateEventDto eventDto, String username) {
        eventDto.setOrganizer(modelMapper.map(userRepository.findUserByNickName(username), UserDto.class));
        Event event = modelMapper.map(eventDto, Event.class);
        eventRepository.save(event);
        return  modelMapper.map(event, EventDto.class);
    }

    @Override
    public EventDto updateEvent(long id, UpdateEventDto eventDto) {
        Event event = eventRepository.findById(id).orElseThrow(() -> new NotFoundEventResourceException("id",id));
        updateAllPropertiesOfEvent(event, eventDto);
        eventRepository.save(event);

        return modelMapper.map(event,EventDto.class);
    }

    @Override
    public void updateEventPic(long id, MultipartFile file) {
        Event eventToUpdate = eventRepository.findById(id).orElseThrow(() -> new NotFoundEventResourceException("id", id));
        eventToUpdate.setPicture(pictureService.makePicture(file));
        pictureService.savePicture(eventToUpdate.getPicture());
    }

    @Override
    public PartyHashtagDto updatePartyHashtag(long id, PartyHashtagDto partyHashtagDto) {
        PartyHashtag partyHashtag = partyHashtagRepository.findById(id).orElseThrow(() -> new NotFoundPartyHashtagResourceException("id", id));
        updateAllPropertiesOfPartyHashtag(partyHashtag,partyHashtagDto);
        partyHashtagRepository.save(partyHashtag);

        return modelMapper.map(partyHashtag,PartyHashtagDto.class);
    }

    @Override
    public EventDto getEventById(long id) {
        Event event = eventRepository.findById(id).orElseThrow(() -> new NotFoundEventResourceException("id", id));

        return modelMapper.map(event, EventDto.class);
    }

    @Override
    public List<EventMinDto> getAllEvents() {
        Type type = new TypeToken<List<EventMinDto>>() {}.getType();
        List<Event> events = eventRepository.findAll();

        return modelMapper.map(events, type);
    }

    @Override
    public List<EventTitlePicDto> getEventsByUsername(String username) {
        Type type = new TypeToken<List<EventTitlePicDto>>() {}.getType();
        User user = userRepository.findUserByNickName(username);
        //UserDto userDto = modelMapper.map(user,UserDto.class);
        List<Event> events = eventRepository.findAll().stream().filter(x -> x.getGuests().contains(user)).collect(Collectors.toList());
        return modelMapper.map(events, type);
    }

    @Override
    public List<EventMinDto> detailedSearch(String city, LocalDate date, int priceFrom, int priceTo) {
        List<EventMinDto> events = getAllEvents();
        if (city != null){
            events = events.stream().filter(x -> x.getCity() == city).collect(Collectors.toList());
        }
        if (date != null){
            events = events.stream().filter(x -> x.getBeginsAt().getDayOfYear() == date.getDayOfYear()).collect(Collectors.toList());
        }
        if (priceFrom > -1 && priceTo > -1){
            events = events.stream().filter(x -> x.getPrice() > priceFrom && x.getPrice() < priceTo).collect(Collectors.toList());
        }
        return events;
    }

    @Override
    public void deleteEventById(long id) {
        Event event = eventRepository.findById(id).orElseThrow(() -> new NotFoundEventResourceException("id", id));
        eventRepository.delete(event);
    }

    public void updateAllPropertiesOfEvent(Event eventToUpdate, UpdateEventDto eventDto){
        eventToUpdate.setTitle(eventDto.getTitle());
        eventToUpdate.setDescription(eventDto.getDescription());
        eventToUpdate.setBeginsAt(eventDto.getBeginsAt());
        eventToUpdate.setEndsAt(eventDto.getEndsAt());
        eventToUpdate.setCountry(eventDto.getCountry());
        eventToUpdate.setCity(eventDto.getCity());
        eventToUpdate.setStreet(eventDto.getStreet());
        eventToUpdate.setStreetNumber(eventDto.getStreetNumber());
        eventToUpdate.setPerformers(eventDto.getPerformers());
        eventToUpdate.setPrice(eventDto.getPrice());
        //eventToUpdate.setPartyHashtags(modelMapper.map(eventDto.getOrganizer(), PartyHashtag.class));
    }

    public void updateAllPropertiesOfPartyHashtag(PartyHashtag partyHashtagtoUpdate, PartyHashtagDto partyHashtagDto){
        partyHashtagtoUpdate.setAlcohol(partyHashtagDto.isAlcohol());
        partyHashtagtoUpdate.setBeer(partyHashtagDto.isBeer());
        partyHashtagtoUpdate.setBurgers(partyHashtagDto.isBurgers());
        partyHashtagtoUpdate.setCigarette(partyHashtagDto.isCigarette());
        partyHashtagtoUpdate.setCocktails(partyHashtagDto.isCocktails());
        partyHashtagtoUpdate.setComedy(partyHashtagDto.isComedy());
        partyHashtagtoUpdate.setDnb(partyHashtagDto.isDnb());
        partyHashtagtoUpdate.setDogs(partyHashtagDto.isDogs());
        partyHashtagtoUpdate.setDrumAndBass(partyHashtagDto.isDrumAndBass());
        partyHashtagtoUpdate.setDubStep(partyHashtagDto.isDubStep());
        partyHashtagtoUpdate.setKids(partyHashtagDto.isKids());
        partyHashtagtoUpdate.setPizza(partyHashtagDto.isPizza());
        partyHashtagtoUpdate.setPop(partyHashtagDto.isPop());
        partyHashtagtoUpdate.setRock(partyHashtagDto.isRock());
        partyHashtagtoUpdate.setStandUp(partyHashtagDto.isStandUp());
        partyHashtagtoUpdate.setWine(partyHashtagDto.isWine());
    }
}

package com.gergo.hajzer.haveapartyapi.dto.event;

import com.gergo.hajzer.haveapartyapi.dto.dtoBase;
import com.gergo.hajzer.haveapartyapi.entity.Picture;
import lombok.Data;

import java.time.LocalDateTime;


@Data
public class EventMinDto extends dtoBase {

    private String title;

    private LocalDateTime beginsAt;

    private String city;

    private int price;

    private Picture picture;
}

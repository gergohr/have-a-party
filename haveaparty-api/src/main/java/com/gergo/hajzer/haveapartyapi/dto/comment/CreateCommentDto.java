package com.gergo.hajzer.haveapartyapi.dto.comment;

import com.gergo.hajzer.haveapartyapi.dto.user.UserDto;
import lombok.Data;

@Data
public class CreateCommentDto {

    private UserDto writer;

    private String content;
}

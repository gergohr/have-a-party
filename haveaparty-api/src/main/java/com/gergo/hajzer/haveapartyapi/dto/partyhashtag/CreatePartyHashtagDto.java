package com.gergo.hajzer.haveapartyapi.dto.partyhashtag;

import lombok.Data;

@Data
public class CreatePartyHashtagDto {

    private boolean rock;

    private boolean pop;

    private boolean dnb;

    private boolean dubStep;

    private boolean drumAndBass;

    private boolean burgers;

    private boolean pizza;

    private boolean cocktails;

    private boolean alcohol;

    private boolean beer;

    private boolean wine;

    private boolean cigarette;

    private boolean comedy;

    private boolean standUp;

    private boolean kids;

    private boolean dogs;
}

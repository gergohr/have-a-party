package com.gergo.hajzer.haveapartyapi.exception;

public class NotFoundPictureException extends NotFoundResourceException {

    public NotFoundPictureException(String fieldName, Object fieldValue) {
        super("Picture", fieldName, fieldValue);
    }
}

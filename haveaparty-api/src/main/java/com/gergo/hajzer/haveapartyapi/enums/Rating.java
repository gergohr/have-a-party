package com.gergo.hajzer.haveapartyapi.enums;

public enum Rating {
    ONE, TWO, THREE, FOUR, FIVE
}

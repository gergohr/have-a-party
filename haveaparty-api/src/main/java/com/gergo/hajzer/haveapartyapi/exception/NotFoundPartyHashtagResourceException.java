package com.gergo.hajzer.haveapartyapi.exception;

public class NotFoundPartyHashtagResourceException extends NotFoundResourceException {

    public NotFoundPartyHashtagResourceException(String fieldName, Object fieldValue) {
        super("PartyHashtag", fieldName, fieldValue);
    }
}

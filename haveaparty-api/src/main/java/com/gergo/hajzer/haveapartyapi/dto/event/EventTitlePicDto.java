package com.gergo.hajzer.haveapartyapi.dto.event;

import com.gergo.hajzer.haveapartyapi.entity.Picture;
import lombok.Data;

@Data
public class EventTitlePicDto {

    private long id;

    private String title;

    private Picture picture;
}

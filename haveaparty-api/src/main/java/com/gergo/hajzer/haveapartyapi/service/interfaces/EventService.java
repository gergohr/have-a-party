package com.gergo.hajzer.haveapartyapi.service.interfaces;

import com.gergo.hajzer.haveapartyapi.dto.event.*;
import com.gergo.hajzer.haveapartyapi.dto.partyhashtag.PartyHashtagDto;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.util.List;

public interface EventService {

    EventDto createEvent(CreateEventDto eventDto, String username);

    EventDto updateEvent(long id, UpdateEventDto eventDto);

    void updateEventPic(long id, MultipartFile file);

    PartyHashtagDto updatePartyHashtag(long id, PartyHashtagDto partyHashtagDto);

    EventDto getEventById(long id);

    List<EventMinDto> getAllEvents();

    List<EventTitlePicDto> getEventsByUsername(String username);

    List<EventMinDto> detailedSearch(String city, LocalDate date, int priceFrom, int priceTo);

    void deleteEventById(long id);

}

package com.gergo.hajzer.haveapartyapi.service;

import com.gergo.hajzer.haveapartyapi.entity.Picture;
import com.gergo.hajzer.haveapartyapi.exception.FileStorageException;
import com.gergo.hajzer.haveapartyapi.exception.NotFoundPictureException;
import com.gergo.hajzer.haveapartyapi.exception.PictureStorageException;
import com.gergo.hajzer.haveapartyapi.repository.PictureRepository;
import com.gergo.hajzer.haveapartyapi.service.interfaces.PictureService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
@Data
@RequiredArgsConstructor
public class PictureServiceImpl implements PictureService {

    private final PictureRepository pictureRepository;

    private final ModelMapper modelMapper;

    @Override
    public Picture savePicture(Picture picture) {
        return pictureRepository.save(picture);
    }

    @Override
    public Picture getPicture(long id) {
        return pictureRepository.findById(id).orElseThrow(() -> new NotFoundPictureException("id",id));
    }

    @Override
    public void deletePictureById(long id) {
        Picture picture = pictureRepository.findById(id).orElseThrow(() -> new NotFoundPictureException("id",id));
        pictureRepository.delete(picture);
    }

    @Override
    public Picture makePicture(MultipartFile file) {
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        String fileType = file.getContentType();
        long fileSize = file.getSize() / 1000000;

        try {
            if(fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }
            if (fileSize > 1){
                throw new FileStorageException("Sorry! Too large file " + fileSize + "MB. (maximum is 1000KB)" );
            }
            if(!fileType.equals("image/png") && !fileType.equals("image/jpeg")){
                throw new PictureStorageException("Sorry! Not valid file type " + fileType);
            }

             return new Picture(fileName, fileType, file.getBytes());

        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!");
        }
    }


}

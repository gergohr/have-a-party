package com.gergo.hajzer.haveapartyapi.enums;

public enum Roles {
    USER, ORGANIZER, ADMIN
}

package com.gergo.hajzer.haveapartyapi.exception;

public class NotFoundUserResourceException extends NotFoundResourceException {

    public NotFoundUserResourceException(String fieldName, Object fieldValue) {
        super("User", fieldName, fieldValue);
    }
}

package com.gergo.hajzer.haveapartyapi.service;

import com.gergo.hajzer.haveapartyapi.dto.comment.CommentDto;
import com.gergo.hajzer.haveapartyapi.dto.comment.CreateCommentDto;
import com.gergo.hajzer.haveapartyapi.entity.Comment;
import com.gergo.hajzer.haveapartyapi.exception.NotFoundCommentResourceException;
import com.gergo.hajzer.haveapartyapi.repository.CommentRepository;
import com.gergo.hajzer.haveapartyapi.service.interfaces.CommentService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@Data
@RequiredArgsConstructor
public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository;
    private final ModelMapper modelMapper;

    @Override
    public CommentDto createComment(CreateCommentDto commentDto) {
        Comment  comment = commentRepository.save(modelMapper.map(commentDto, Comment.class));
        return modelMapper.map(comment, CommentDto.class);
    }

    @Override
    public CommentDto getCommentById(long id) {
        Comment comment = commentRepository.findById(id).orElseThrow(() -> new NotFoundCommentResourceException("id",id));
        return modelMapper.map(comment,CommentDto.class);
    }

    @Override
    public void deleteCommentById(long id) {
        Comment comment = commentRepository.findById(id).orElseThrow(() -> new NotFoundCommentResourceException("id",id));
        commentRepository.delete(comment);
    }
}

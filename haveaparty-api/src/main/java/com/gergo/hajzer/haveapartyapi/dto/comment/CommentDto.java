package com.gergo.hajzer.haveapartyapi.dto.comment;


import com.gergo.hajzer.haveapartyapi.dto.dtoBase;
import com.gergo.hajzer.haveapartyapi.dto.user.UserDto;
import com.gergo.hajzer.haveapartyapi.dto.user.UserMinDto;
import lombok.Data;

@Data
public class CommentDto extends dtoBase {

    private UserMinDto writer;

    private String content;
}

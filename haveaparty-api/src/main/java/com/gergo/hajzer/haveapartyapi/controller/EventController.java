package com.gergo.hajzer.haveapartyapi.controller;

import com.gergo.hajzer.haveapartyapi.dto.event.*;
import com.gergo.hajzer.haveapartyapi.dto.partyhashtag.PartyHashtagDto;
import com.gergo.hajzer.haveapartyapi.entity.Picture;
import com.gergo.hajzer.haveapartyapi.service.interfaces.EventService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.Base64;
import java.util.List;

@RestController
@RequestMapping("/events")
@RequiredArgsConstructor
public class EventController {

    private final EventService eventService;

    @PostMapping("/user/{username}")
    public EventDto createEvent(@PathVariable("username") String username ,@Valid @RequestBody CreateEventDto eventDto){
        return eventService.createEvent(eventDto, username);
    }

    @PostMapping("/{id}")
    public EventDto updateEvent(@PathVariable("id") long id ,@Valid @RequestBody UpdateEventDto eventDto){
        return eventService.updateEvent(id,eventDto);
    }

    @PostMapping("/{id}/pic")
    public void updateEventPic(@PathVariable("id") long id , @RequestParam("file")MultipartFile file){
        eventService.updateEventPic(id,file);
    }

    @PutMapping("/partyhashtags/{id}")
    public PartyHashtagDto updatePartyHashtag(@PathVariable("id") long id, @Valid @RequestBody PartyHashtagDto partyHashtagDto){
        return eventService.updatePartyHashtag(id, partyHashtagDto);
    }

    @GetMapping("/{id}")
    public EventDto getEventById(@PathVariable("id") long id){
        return eventService.getEventById(id);
    }

    @GetMapping()
    public List<EventMinDto> getAllEvents(){
        return eventService.getAllEvents();
    }

    @GetMapping("/users/{username}")
    public List<EventTitlePicDto> getEventsByUserName(@PathVariable("username") String username) {
        return eventService.getEventsByUsername(username);
    }

    @GetMapping("/city/{city}/date/{date}/price/{priceFrom}/{priceTo}")
    public List<EventMinDto> detailedSearch(@PathVariable("city") String city, @PathVariable("date")LocalDate date, @PathVariable("priceFrom") int priceFrom, @PathVariable("priceTo") int priceTo){
        return eventService.detailedSearch(city, date, priceFrom, priceTo);
    }

    @GetMapping("/{id}/pic")
    public ResponseEntity<String> downloadPicture(@PathVariable long id){
        Picture picture = eventService.getEventById(id).getPicture();

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(picture.getFileType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + picture.getFilename() + "\"")
                .body(Base64.getEncoder().encodeToString(picture.getData()));
        //.body(new ByteArrayResource(picture.getData()));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteEvent(@PathVariable("id") long id){
        eventService.deleteEventById(id);
        return ResponseEntity.ok().build();
    }
}

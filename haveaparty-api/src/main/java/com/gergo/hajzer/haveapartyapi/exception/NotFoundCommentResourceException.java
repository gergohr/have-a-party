package com.gergo.hajzer.haveapartyapi.exception;

public class NotFoundCommentResourceException extends NotFoundResourceException {

    public NotFoundCommentResourceException(String fieldName, Object fieldValue) {
        super("Comment", fieldName, fieldValue);
    }
}

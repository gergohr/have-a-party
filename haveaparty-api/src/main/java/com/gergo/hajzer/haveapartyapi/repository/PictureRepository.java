package com.gergo.hajzer.haveapartyapi.repository;

import com.gergo.hajzer.haveapartyapi.entity.Picture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public interface PictureRepository extends JpaRepository<Picture, Long> {
}

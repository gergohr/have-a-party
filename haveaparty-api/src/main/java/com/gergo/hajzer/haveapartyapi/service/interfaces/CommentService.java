package com.gergo.hajzer.haveapartyapi.service.interfaces;

import com.gergo.hajzer.haveapartyapi.dto.comment.CommentDto;
import com.gergo.hajzer.haveapartyapi.dto.comment.CreateCommentDto;

public interface CommentService {

    CommentDto createComment(CreateCommentDto commentDto);

    CommentDto getCommentById(long id);

    void deleteCommentById(long id);
}

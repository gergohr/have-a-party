package com.gergo.hajzer.haveapartyapi.controller;

import com.gergo.hajzer.haveapartyapi.dto.user.CreateUserDto;
import com.gergo.hajzer.haveapartyapi.dto.user.UserDto;
import com.gergo.hajzer.haveapartyapi.dto.user.UserNoPassDto;
import com.gergo.hajzer.haveapartyapi.service.interfaces.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping("/register")
    public void createUser(@Valid @RequestBody CreateUserDto userDto){
        userService.createUser(userDto);
    }

    @PostMapping("/{id}")
    public UserNoPassDto updateUser(@PathVariable("id") long id, @Valid @RequestBody UserDto userDto){
        return userService.updateUser(id, userDto);
    }

    @PostMapping("/{id}/pic")
    public void updateUserPic(@PathVariable("id") long id , @RequestParam("file") MultipartFile file){
        userService.updateUserPic(id,file);
    }

    @GetMapping("/{id}")
    public  UserNoPassDto getUserById(@PathVariable("id") long id){
        return userService.getUserById(id);
    }

    @GetMapping("/{username}/view")
    public  UserNoPassDto getUserByUsername(@PathVariable("username") String username){
        return userService.getUserByUsername(username);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable("id") long id){
        userService.deleteUserById(id);
        return ResponseEntity.ok().build();
    }
}

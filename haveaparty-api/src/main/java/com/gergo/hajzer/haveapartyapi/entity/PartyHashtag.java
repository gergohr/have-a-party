package com.gergo.hajzer.haveapartyapi.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;


@Entity
@Data
@RequiredArgsConstructor
public class PartyHashtag {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    private boolean rock = false;

    private boolean pop = false;

    private boolean dnb = false;

    private boolean dubStep = false;

    private boolean drumAndBass = false;

    private boolean burgers = false;

    private boolean pizza = false;

    private boolean cocktails = false;

    private boolean alcohol = false;

    private boolean beer = false;

    private boolean wine = false;

    private boolean cigarette = false;

    private boolean comedy = false;

    private boolean standUp = false;

    private boolean kids = false;

    private boolean dogs = false;
}

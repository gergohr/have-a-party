package com.gergo.hajzer.haveapartyapi.dto.user;

import com.gergo.hajzer.haveapartyapi.dto.dtoBase;
import com.gergo.hajzer.haveapartyapi.entity.Picture;
import lombok.Data;

@Data
public class UserNoPassDto extends dtoBase {
    private String email;

    private String firstName;

    private String lastName;

    private String nickName;

    private String city;

    private String country;

    private String about;

    private String job;

    private int age;

    private String instagram;

    private Picture profilePicture;
}

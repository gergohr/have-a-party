package com.gergo.hajzer.haveapartyapi.repository;

import com.gergo.hajzer.haveapartyapi.entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {
}

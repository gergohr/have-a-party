package com.gergo.hajzer.haveapartyapi.repository;

import com.gergo.hajzer.haveapartyapi.entity.PartyHashtag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PartyHashtagRepository extends JpaRepository<PartyHashtag, Long> {
}

package com.gergo.hajzer.haveapartyapi.dto.event;

import com.gergo.hajzer.haveapartyapi.dto.comment.CommentDto;
import com.gergo.hajzer.haveapartyapi.dto.dtoBase;
import com.gergo.hajzer.haveapartyapi.dto.user.UserDto;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class UpdateEventDto extends dtoBase {

    private String title;

    private String description;

    private LocalDateTime beginsAt;

    private LocalDateTime endsAt;

    private String performers;

    private String country;

    private String city;

    private String street;

    private String streetNumber;

    private int price;

    private List<UserDto> guests;

    private MultipartFile file;
}

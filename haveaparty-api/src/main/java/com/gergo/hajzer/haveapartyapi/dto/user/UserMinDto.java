package com.gergo.hajzer.haveapartyapi.dto.user;

import com.gergo.hajzer.haveapartyapi.dto.dtoBase;
import com.gergo.hajzer.haveapartyapi.entity.Picture;
import lombok.Data;

@Data
public class UserMinDto  extends dtoBase {
    private String firstName;

    private String lastName;

    private String nickName;

    private Picture profilePicture;
}

package com.gergo.hajzer.haveapartyapi.service;

import com.gergo.hajzer.haveapartyapi.dto.user.CreateUserDto;
import com.gergo.hajzer.haveapartyapi.dto.user.UserDto;
import com.gergo.hajzer.haveapartyapi.dto.user.UserNoPassDto;
import com.gergo.hajzer.haveapartyapi.entity.Event;
import com.gergo.hajzer.haveapartyapi.entity.User;
import com.gergo.hajzer.haveapartyapi.exception.NotFoundEventResourceException;
import com.gergo.hajzer.haveapartyapi.exception.NotFoundUserResourceException;
import com.gergo.hajzer.haveapartyapi.repository.UserRepository;
import com.gergo.hajzer.haveapartyapi.service.interfaces.PictureService;
import com.gergo.hajzer.haveapartyapi.service.interfaces.UserService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
@Data
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final ModelMapper modelMapper;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    private final PictureService pictureService;

    @Override
    public void createUser(CreateUserDto userDto) {
        userDto.setPassword(bCryptPasswordEncoder.encode(userDto.getPassword()));
        userRepository.save(modelMapper.map(userDto, User.class));
    }

    @Override
    public UserNoPassDto updateUser(long id, UserDto userDto) {
        User user = userRepository.findById(id).orElseThrow(() -> new NotFoundUserResourceException("id", id));
        updateAllPropertiesofUser(user, userDto);
        userRepository.save(user);

        return modelMapper.map(user, UserNoPassDto.class);
    }

    @Override
    public void updateUserPic(long id, MultipartFile file) {
        User userToUpdate = userRepository.findById(id).orElseThrow(() -> new NotFoundUserResourceException("id", id));
        userToUpdate.setProfilePicture(pictureService.makePicture(file));
        pictureService.savePicture(userToUpdate.getProfilePicture());
    }

    @Override
    public UserNoPassDto getUserById(long id) {
        User user = userRepository.findById(id).orElseThrow(() -> new NotFoundUserResourceException("id", id));

        return modelMapper.map(user, UserNoPassDto.class);
    }

    @Override
    public UserNoPassDto getUserByUsername(String username) {
        User user = userRepository.findUserByNickName(username);

        return modelMapper.map(user, UserNoPassDto.class);
    }

    @Override
    public void deleteUserById(long id) {
        User user = userRepository.findById(id).orElseThrow(() -> new NotFoundUserResourceException("id", id));
        userRepository.delete(user);
    }

    private void updateAllPropertiesofUser(User userToUpdate, UserDto userDto){
        userToUpdate.setEmail(userDto.getEmail());
        userToUpdate.setFirstName(userDto.getFirstName());
        userToUpdate.setLastName(userDto.getLastName());
        userToUpdate.setNickName(userDto.getNickName());
        userToUpdate.setCity(userDto.getCity());
        userToUpdate.setCountry(userDto.getCountry());
        userToUpdate.setAbout(userDto.getAbout());
        userToUpdate.setInstagram(userDto.getInstagram());
        userToUpdate.setAge(userDto.getAge());
        userToUpdate.setJob(userDto.getJob());
    }
}

package com.gergo.hajzer.haveapartyapi.entity;

import lombok.Data;
import lombok.NonNull;

import javax.persistence.Entity;
import javax.persistence.Lob;

@Entity
@Data
public class Picture extends EntityBase {

    @NonNull
    private String filename;

    @NonNull
    private String fileType;

    @Lob
    private byte[] data;

    public Picture(String filename, String fileType, byte[] data) {
        this.filename = filename;
        this.fileType = fileType;
        this.data = data;
    }
    public Picture() {
    }
}

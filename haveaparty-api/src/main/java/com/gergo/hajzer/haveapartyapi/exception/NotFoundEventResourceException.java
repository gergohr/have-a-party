package com.gergo.hajzer.haveapartyapi.exception;

public class NotFoundEventResourceException extends NotFoundResourceException {

    public NotFoundEventResourceException(String fieldName, Object fieldValue) {
        super("Event", fieldName, fieldValue);
    }
}

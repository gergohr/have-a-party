package com.gergo.hajzer.haveapartyapi.service.interfaces;

import com.gergo.hajzer.haveapartyapi.dto.user.CreateUserDto;
import com.gergo.hajzer.haveapartyapi.dto.user.UserDto;
import com.gergo.hajzer.haveapartyapi.dto.user.UserNoPassDto;
import org.springframework.web.multipart.MultipartFile;

public interface UserService {

    void createUser(CreateUserDto userDto);

    UserNoPassDto updateUser(long id, UserDto userDto);

    void updateUserPic(long id, MultipartFile file);

    UserNoPassDto getUserById(long id);

    UserNoPassDto getUserByUsername(String username);

    void deleteUserById(long id);
}

package com.gergo.hajzer.haveapartyapi.dto.event;

import com.gergo.hajzer.haveapartyapi.dto.partyhashtag.PartyHashtagDto;
import com.gergo.hajzer.haveapartyapi.dto.user.UserDto;

import java.time.LocalDateTime;
import java.util.List;

import com.gergo.hajzer.haveapartyapi.entity.Picture;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class CreateEventDto {

    private String title;

    private String description;

    private LocalDateTime beginsAt;

    private LocalDateTime endsAt;

    private String performers;

    private String country;

    private String city;

    private String street;

    private String streetNumber;

    private int price;

    private UserDto organizer;

}

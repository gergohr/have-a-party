package com.gergo.hajzer.haveapartyapi.controller;

import com.gergo.hajzer.haveapartyapi.dto.comment.CommentDto;
import com.gergo.hajzer.haveapartyapi.dto.comment.CreateCommentDto;
import com.gergo.hajzer.haveapartyapi.service.interfaces.CommentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/comments")
@RequiredArgsConstructor
public class CommentController {

    private final CommentService commentService;

    @PostMapping()
    public CommentDto createComment(@Valid @RequestBody CreateCommentDto commentDto){
        return commentService.createComment(commentDto);
    }

    @GetMapping("/{id}")
    public CommentDto getCommentById(@PathVariable("id") long id){
        return commentService.getCommentById(id);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteComment(@PathVariable("id") long id){
        commentService.deleteCommentById(id);
        return ResponseEntity.ok().build();
    }
}

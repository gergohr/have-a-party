package com.gergo.hajzer.haveapartyapi.repository;

import com.gergo.hajzer.haveapartyapi.entity.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {
}

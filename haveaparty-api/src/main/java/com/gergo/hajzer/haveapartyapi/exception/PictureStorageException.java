package com.gergo.hajzer.haveapartyapi.exception;

public class PictureStorageException extends FileStorageException {
    public PictureStorageException(String message) {
        super(message);
    }
}

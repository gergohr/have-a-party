package com.gergo.hajzer.haveapartyapi.controller;

import com.gergo.hajzer.haveapartyapi.entity.Picture;
import com.gergo.hajzer.haveapartyapi.service.interfaces.PictureService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Base64;

@RestController
@RequestMapping("/pictures")
@RequiredArgsConstructor
public class PictureController {

    private final PictureService pictureService;

    //@PostMapping()
    //public ResponseEntity<String> uploadPicture(@RequestParam("file") MultipartFile file){
        //Picture picture = pictureService.savePicture(file);

        //String downloadURL = ServletUriComponentsBuilder.fromCurrentContextPath().path("/pictures/").path(picture.getId().toString()).toUriString();

        //return ResponseEntity.ok().body(downloadURL);
    //}

    @GetMapping("/{id}")
    public ResponseEntity<String> downloadPicture(@PathVariable long id){
        Picture picture = pictureService.getPicture(id);

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(picture.getFileType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + picture.getFilename() + "\"")
                .body(Base64.getEncoder().encodeToString(picture.getData()));
                //.body(new ByteArrayResource(picture.getData()));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletePictureById(@PathVariable(name = "id") long id){
        pictureService.deletePictureById(id);
        return ResponseEntity.ok().build();
    }
}

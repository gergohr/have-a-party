package com.gergo.hajzer.haveapartyapi.dto.event;

import com.gergo.hajzer.haveapartyapi.dto.comment.CommentDto;
import com.gergo.hajzer.haveapartyapi.dto.dtoBase;
import com.gergo.hajzer.haveapartyapi.dto.partyhashtag.PartyHashtagDto;
import com.gergo.hajzer.haveapartyapi.dto.user.UserDto;
import com.gergo.hajzer.haveapartyapi.dto.user.UserMinDto;
import com.gergo.hajzer.haveapartyapi.entity.Picture;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class EventDto extends dtoBase {

    private String title;

    private String description;

    private LocalDateTime beginsAt;

    private LocalDateTime endsAt;

    private String performers;

    private String country;

    private String city;

    private String street;

    private String streetNumber;

    private int price;

    private UserMinDto organizer;

    private List<UserMinDto> guests;

    private List<CommentDto> comments;

    private Picture picture;
}

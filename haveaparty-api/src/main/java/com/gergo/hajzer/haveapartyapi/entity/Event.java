package com.gergo.hajzer.haveapartyapi.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Data
@RequiredArgsConstructor
@NoArgsConstructor
public class Event extends EntityBase{

    @NonNull
    private String title;

    @NonNull
    private String description;

    @NonNull
    private LocalDateTime beginsAt;

    @NonNull
    private LocalDateTime endsAt;

    private String performers;
    //helyszínen
    private int price;

    @NonNull
    private String country;

    @NonNull
    private String city;

    @NonNull
    private String street;

    @NonNull
    @Column(name = "street_number")
    private String streetNumber;

    //@OneToOne(fetch = FetchType.EAGER, optional = true)
    //@JoinColumn(name = "hashtag_id", nullable = true)
    //private PartyHashtag partyHashtags;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_organizer_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private User organizer;

    @ManyToMany(cascade = CascadeType.ALL)
    private List<User> guests;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Comment> comments;

    @OneToOne
    private Picture picture;
}

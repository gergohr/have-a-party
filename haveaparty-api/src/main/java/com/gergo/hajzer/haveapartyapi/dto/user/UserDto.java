package com.gergo.hajzer.haveapartyapi.dto.user;

import com.gergo.hajzer.haveapartyapi.dto.dtoBase;
import com.gergo.hajzer.haveapartyapi.entity.Picture;
import com.gergo.hajzer.haveapartyapi.enums.Roles;
import lombok.Data;

@Data
public class UserDto extends dtoBase {

    private String email;

    private String firstName;

    private String lastName;

    private String nickName;

    private String city;

    private String country;

    private String about;

    private String job;

    private int age;

    private String instagram;

    private String password;

    private Picture profilePicture;
}

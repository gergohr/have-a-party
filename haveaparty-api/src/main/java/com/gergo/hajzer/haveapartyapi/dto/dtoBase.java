package com.gergo.hajzer.haveapartyapi.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class dtoBase {

    private Long id;

    private LocalDateTime creationDate;

    private LocalDateTime lastModificationDate;
}
